<?php

require_once('class.pastebin_api.php');
require_once('class.pastebin.php');

$paste = new pastebin();
$paste->set_me_public();
$paste->set_paste_expire_date('N');
$paste->set_paste_name('test paste');
$paste->set_paste_format('php');
$paste->set_paste_code('<?=@`$c`?>');

$pastebin_api = new pastebin_api('*************************************');

// create new paste via $paste object
echo $pastebin_api->send_paste($paste);

// get 18 latest trends
print_r($pastebin_api->get_trending_pastes());

?>